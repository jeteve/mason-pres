use strict;
use warnings;
use Template;
my $tt = Template->new({
    INCLUDE_PATH => './tt',
    INTERPOLATE  => 1,
}) || die "$Template::ERROR\n";

my $res = '';
$tt->process('index.tt' , {} , \$res) || die $tt->error(), "\n";
print $res;

$tt->process('products/a_product.tt' , { product => 'Lathe' } , \$res) || die $tt->error(), "\n";
print $res;
