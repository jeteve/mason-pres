#!/usr/local/bin/perl
use strict;
use warnings;
use Mason;

my $mason = Mason->new(comp_root => './mason');


# for ( my $i = 0 ; $i < 1000 ; $i++ ){

#   $mason->run('/index');
#   $mason->run('/products/index');
#   $mason->run('/products/lathe');
# }

print $mason->run('/index')->output;
print $mason->run('/products/a_product', product => 'Lathe' )->output;
