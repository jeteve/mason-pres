use strict;
use Benchmark qw/:all/;
use Cwd;
use Mason;
use HTML::Mason;
use Template;

my $mason = Mason->new( comp_root => './mason' , static_source => 1, data_dir => '/tmp/mason_jerome');

my $outbuf;

my $html_mason = HTML::Mason::Interp->new( comp_root => getcwd().'/html_mason' , static_source => 1 , data_dir => '/tmp/html_mason_jerome/' , out_method => \$outbuf);

my $tt = Template->new( {
                         INCLUDE_PATH => './tt',
                         INTERPOLATE  => 1,
                         STRICT => 1,
                         STAT_TTL => 120,
                         COMPILE_EXT => '.ttc',
                         COMPILE_DIR => '/tmp/tt_jerome'
                        }) || die "$Template::ERROR\n";

my $mason_run = sub{
   my $index = $mason->run('/index')->output;
   my $product = $mason->run('/products/a_product', product => 'Lathe' )->output;
   ## print $index.$product;
};

my $html_mason_run = sub{
    $outbuf = '';
    $html_mason->exec('/index');
    $outbuf = '';
    $html_mason->exec('/products/a_product' , product => 'Lathe' );
};

my $template_run = sub{
  my $index = '';
  $tt->process('index.tt' , {} , \$index) || die $tt->error(), "\n";
  my $product = '';
  $tt->process('products/a_product.tt' , { product => 'Lathe' } , \$product) || die $tt->error(), "\n";
  ## print $index.$product;
};

&$mason_run();
&$html_mason_run();
&$template_run();

cmpthese(1000,
	 {
	     'Mason1' => $html_mason_run,
	     'Mason2' => $mason_run,
	     'TT' => $template_run,
	 });
